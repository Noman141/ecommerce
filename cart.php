<?php include "inc/header.php"?>
<?php
if (isset($_GET['delPro'])){
    $id =preg_replace('/[^-a-zA-Z0-9_]/','',$_GET['delPro']);
    $delPro = $cart->deleteProductByCart($id);
}
?>
<?php 
   if ($_SERVER["REQUEST_METHOD"] == "POST"){
   	       $cartId = $_POST['cartId'];
           $quantity = $_POST['quantity'];
           $updateCart = $cart->updateCartQuantity($cartId,$quantity);
           if ($quantity <= 0){
               $delPro = $cart->deleteProductByCart($cartId);
           }
    }
?>
<?php
    if (!isset($_GET['id'])){
        echo "<meta http-equiv = 'refresh' content = 0;URL=?id =live' />";
    }
?>
 <div class="main">
    <div class="content">
    	<div class="cartoption">		
			<div class="cartpage">
			    	<h2>Your Cart</h2>
                <?php
                   if (isset($updateCart)){
                       echo $updateCart;
                   }

                   if (isset($delPro)){
                       echo $delPro;
                   }
                ?>
						<table class="tblone">
							<tr>
                                <th width="5%">No.</th>
								<th width="30%">Product Name</th>
								<th width="20%">Image</th>
								<th width="10%">Price</th>
								<th width="20%">Quantity</th>
								<th width="20%">Total Price</th>
								<th width="5%">Action</th>
							</tr>
                            <?php
                                $getCPro = $cart->getCartProduct();
                                if ($getCPro){
                                    $i = 0;
                                    $quantity = 0;
                                    $sum = 0;
                                    while ($result = $getCPro->fetch_assoc()){
                                        $i++;
                            ?>
							<tr>
                                <td><?php echo $i;?></td>
								<td><?php echo $result['productName'];?></td>
								<td><img src="admin/<?php echo $result['image'];?>" alt=""/></td>
								<td>$<?php echo $result['price'];?></td>
								<td>
									<form action="" method="post">
										<input type="hidden" name="cartId" value="<?php echo $result['cartId'];?>"/>
										<input type="number" name="quantity" value="<?php echo $result['quantity'];?>"/>
										<input type="submit" name="submit" value="Update"/>
									</form>
								</td>
                                <td>$<?php
                                    $total = $result['price'] * $result['quantity'];
                                    echo $total;
                                    ?></td>
								<td><a onclick="return confirm('Are You Sure To Remove!')" href="?delPro=<?php echo $result['cartId'];?>">X</a></td>
							</tr>
                             <?php
                                 $quantity = $quantity + $result['quantity'];
                                 $sum = $sum + $total;
                                 Session::set("sum",$sum);
                                 Session::set("quantity",$quantity);
                             ?>
							<?php } }?>
						</table>
                        <?php
                        $getData = $cart->checkCartTable();
                        if ($getData){ ?>
						<table style="float:right;text-align:left;" width="40%">
							<tr>
								<th>Sub Total : </th>
								<td>$<?php echo $sum;?></td>
							</tr>
							<tr>
								<th>VAT : (10%)</th>
								<td>$<?php
                                      $vat = $sum * 0.1;
                                      echo $vat;
                                    ?>
                                </td>
							</tr>
							<tr>
								<th>Grand Total :</th>
								<td>$<?php
                                    $grandTotal = $sum + $vat;
                                    echo $grandTotal;
								?></td>
							</tr>
					   </table>
                       <?php }else{
                            echo "<span style='color: red;font-size: 50px;'>Cart Empty</span>";
                        };?>
					</div>
					<div class="shopping">
						<div class="shopleft">
							<a href="index.php"> <img src="images/shop.png" alt="" /></a>
						</div>
						<div class="shopright">
							<a href="payment.php"> <img src="images/check.png" alt="" /></a>
						</div>
					</div>
    	</div>  	
       <div class="clear"></div>
    </div>
 </div>
<?php include "inc/footer.php"?>

