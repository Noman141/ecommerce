<?php include_once 'inc/header.php'?>
<style>
    .notfound {
        min-height: 400px;
        padding-top: 100px;
    }
    .notfound p {
        font-size: 100px;
        font-weight: bold;
        line-height: 137px;
        text-align: center;
    }
    .notfound p span {
        color: #ff0000;
        display: block;
        font-size: 200px;
    }
</style>
<div class="notfound">
    <p><span>404</span> Not Found</p>
</div>
<?php include_once 'inc/footer.php'?>
