<?php include "inc/header.php"?>
<?php
$getLogin = Session::get("cmrLogin");
if ($getLogin == false){
    header("Location:login.php");
}
?>
<?php 
    $cmrId = Session::get("cmrId");
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update'])){
           $updatecustomerdata = $cmr->updateCustomerData($_POST,$cmrId);
       }

?>
    <style>
        .tblone{width: 550px;margin: 0 auto;border: 2px solid #ddd;}
        .tblone tr td {text-align: justify;}
        .tblone input[type="text"]{width:300px;margin: 5px;font-size: 15px;height: 30px;}
    </style>
    <div class="main">
        <div class="content">
            <div class="section-group">
                <?php
                $id = Session::get("cmrId");
                $getData = $cmr->getAllCustomerData($id);

                if ($getData){
                    while ($result = $getData->fetch_assoc()){

                        ?>
                  <form action="" method="post">
                        <table class="tblone">
                            <?php
                                if (isset($updatecustomerdata)){
                                    echo "<tr><td colspan=\"3\" style=\"text-align: center\">$updatecustomerdata</td></tr>";
                            }
                            ?>
                            <tr>
                                <td colspan="3" style="text-align: center"><h2>Update User Profile</h2></td>
                            </tr>
                            <tr>
                                <td width="20%">Name</td>
                                <td width="5%">:</td>
                                <td><input type="text" name="name" value="<?php echo $result['name']?>"></td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>:</td>
                                <td><input type="text" name="city" value="<?php echo $result['city']?>"></td>
                            </tr>
                            <tr>
                                <td>Country</td>
                                <td>:</td>
                                <td><input type="text" name="country" value="<?php echo $result['country']?>"></td>
                            </tr>
                            <tr>
                                <td>Zip Code</td>
                                <td>:</td>
                                <td><input type="text" name="zip" value="<?php echo $result['zip']?>"></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td>:</td>
                                <td><input type="text" name="phone" value="<?php echo $result['phone']?>"></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>:</td>
                                <td><input type="text" name="email" value="<?php echo $result['email']?>"></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>:</td>
                                <td><input type="text" name="adress" value="<?php echo $result['adress']?>"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><input type="submit" name="update" value="Update"></td>
                            </tr>
                        </table>
                    <?php } }?>
                  </form>
            </div>
        </div>
    </div>

<?php include "inc/footer.php"?>