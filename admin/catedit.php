<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
$cat = new Catagory();
if (!isset($_GET['catid']) AND $_GET['catid'] == NULL ){
   echo "<script>window.location = 'catlist.php';</script>";
}else{
   $catid = $_GET['catid'];
}

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $catName = $_POST['catName'];

    $updatecat = $cat->updateCatagory($catName,$catid);
}
?>
    <div class="grid_10">
        <div class="box round first grid">
            <h2>Update Category</h2>
            <div class="block copyblock">
                <?php
                if (isset($updatecat)){
                    echo $updatecat;
                }
                ?>
                <?php
                    $getcat = $cat->getCatById($catid);
                    if ($getcat){
                        while ($result = $getcat->fetch_assoc()){

                ?>
                <form action="" method="post">
                    <table class="form">
                        <tr>
                            <td>
                                <input type="text" name="catName" value="<?php echo $result['catName']?>" class="medium" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="submit" Value="Update" />
                            </td>
                        </tr>
                    </table>
                </form>
             <?php } }else{ ?>
                       <script>window.location = 'catlist.php';</script>
                   <?php } ?>
            </div>
        </div>
    </div>
<?php include 'inc/footer.php';?>