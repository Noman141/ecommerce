<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Brand.php'?>
<?php include '../classes/Catagory.php'?>
<?php include '../classes/Product.php'?>
<?php
$product = new Product();
if (!isset($_GET['editProId']) || $_GET['editProId'] == NULL){
    echo "<script>window.location='productlist.php'</script>";
}else{
    $proId = preg_replace('/[^-a-zA-Z0-9_]/','',$_GET['editProId']);
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submit'])){
    $updateProduct = $product->productUpdate($_POST,$_FILES,$proId);
}
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Update Product</h2>
        <div class="block">
            <?php
            if (isset($updateProduct)){
                echo $updateProduct;
            }
            ?>
            <?php
               $getPro = $product->getAllProductById($proId);

               if ($getPro){
                   while ($allPro = $getPro->fetch_assoc()){

            ?>
            <form action="" method="post" enctype="multipart/form-data">
                <table class="form">

                    <tr>
                        <td>
                            <label>Product Name</label>
                        </td>
                        <td>
                            <input type="text" name="productName" value="<?php echo $allPro['productName']?>" class="medium" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Category</label>
                        </td>
                        <td>
                            <select id="select" name="catId">
                                <option>Select Category</option>
                                <?php
                                $cat = new Catagory();
                                $getCat = $cat->getAllCat();
                                if ($getCat){
                                    while ($getCatName = $getCat->fetch_assoc()){

                                        ?>

                                        <option <?php
                                           if ($allPro['catId'] == $getCatName['catId']){ ?>
                                              selected = 'selected';
                                           <?php }?> value="<?php echo $getCatName['catId']?>"><?php echo $getCatName['catName']?></option>
                                    <?php } }?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Brand</label>
                        </td>
                        <td>
                            <select id="select" name="brandId">
                                <option>Select Brand</option>
                                <?php
                                $brand = new Brand();
                                $getBrand = $brand->getAllBrand();
                                if ($getBrand){
                                    while ($getBrandName = $getBrand->fetch_assoc()){

                                        ?>
                                        <option <?php
                                           if ($allPro['brandId'] == $getBrandName['brandId']){ ?>
                                              selected = 'selected';
                                           <?php }?>value="<?php echo $getBrandName['brandId']?>"><?php echo $getBrandName['brandName']?></option>
                                    <?php } }?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td style="vertical-align: top; padding-top: 9px;">
                            <label>Description</label>
                        </td>
                        <td>
                            <textarea class="tinymce" name="body"><?php echo $allPro['body']?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Price</label>
                        </td>
                        <td>
                            <input type="text" name="price" value="<?php echo $allPro['price']?>" class="medium" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Upload Image</label>
                        </td>
                        <td>
                            <img style="    vertical-align: middle !important;" src="<?php echo $allPro['image']?>" width="80px" height="60px">
                            <input type="file" name="image" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <label>Product Type</label>
                        </td>
                        <td>
                            <select id="select" name="type">
                                <option>Select Type</option>
                                <?php
                                   if ($allPro['type'] == 0){ ?>
                                       <option selected = 'selected' value="0">Featured</option>
                                       <option value="1">Non-Featured</option>
                                 <?php  } else{?>
                                <option value="0">Featured</option>
                                <option selected = 'selected' value="1">Non-Featured</option>
                                 <?php }?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" Value="Update" />
                        </td>
                    </tr>
                </table>
            </form>
            <?php } }?>
        </div>
    </div>
</div>
<!-- Load TinyMCE -->
<script src="js/tiny-mce/jquery.tinymce.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        setupTinyMCE();
        setDatePicker('date-picker');
        $('input[type="checkbox"]').fancybutton();
        $('input[type="radio"]').fancybutton();
    });
</script>
<!-- Load TinyMCE -->
<?php include 'inc/footer.php';?>


