﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php include '../classes/Product.php'?>
<?php include_once '../helpers/Format.php';?>
<?php
   $product = new Product();
   $fm = new Format();
   $getproduct = $product->getAllProduct();

   if (isset($_GET['delProId'])){
       $delProId = $_GET['delProId'];
       $deleteProduct = $product->deleteProduct($delProId);
   }
?>
<div class="grid_10">
    <div class="box round first grid">
        <h2>Post List</h2>
        <div class="block">
            <?php
               if (isset($deleteProduct)){
                  echo $deleteProduct;
               }
            ?>
            <table class="data display datatable" id="example">
			<thead>
				<tr>
                    <th width="5%">Serial</th>
					<th width="10%">Post Title</th>
					<th width="10%">Category</th>
                    <th width="10%">Brand</th>
                    <th width="20%">Description</th>
					<th width="15%">Image</th>
                    <th width="10%">Price</th>
                    <th width="10%">Type</th>
					<th width="10%">Action</th>
				</tr>
			</thead>
			<tbody>
            <?php
               if ($getproduct){
                   $i = 0;
                   while ($allProduct = $getproduct->fetch_assoc()){
                      $i++;

            ?>
				<tr class="odd gradeX">
                    <td><?php echo $i;?></td>
					<td><?php echo $allProduct['productName']?></td>
                    <td><?php echo $allProduct['catName']?></td>
					<td><?php echo $allProduct['brandName']?></td>
                    <td><?php echo $fm->textShorten($allProduct['body'],50)?></td>
					<td ><img style="    vertical-align: middle !important;" src="<?php echo $allProduct['image']?>" width="80px" height="60px"></td>
                    <td><?php echo $allProduct['price']?>$</td>
                    <td>
                        <?php
                            if ($allProduct['type'] == 0){
                                echo 'Featured';
                            }else{
                                echo 'Non-Featured';
                            }
                        ?>
                    </td>
					<td>
                        <a href="editproduct.php?editProId=<?php echo $allProduct['productId']?>">Edit</a> ||
                        <a onclick="return confirm('Are You Sure To Delete!!')" href="?delProId=<?php echo $allProduct['productId']?>">Delete</a>
                    </td>
				</tr>
            <?php } }?>
			</tbody>
		</table>

       </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();
        $('.datatable').dataTable();
		setSidebarHeight();
    });
</script>
<?php include 'inc/footer.php';?>
