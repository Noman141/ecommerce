﻿<?php include 'inc/header.php';?>
<?php include 'inc/sidebar.php';?>
<?php
$cart = new Cart();
$fm = new Format();
?>
<?php
 if (isset($_GET['recieveid'])){
     $id = $_GET['recieveid'];
     $price = $_GET['price'];
     $date = $_GET['time'];
     $receive = $cart->productReceived($id,$price,$date);
 }

if (isset($_GET['removeid'])){
    $id = $_GET['removeid'];
    $price = $_GET['price'];
    $date = $_GET['time'];
    $remove = $cart->productRemove($id,$price,$date);
}
?>
        <div class="grid_10">
            <div class="box round first grid">
                <h2>Inbox</h2>
                <?php
                  if (isset($receive)){
                      echo $receive;
                  }
                if (isset($remove)){
                    echo $remove;
                }
                ?>

                <div class="block">        
                    <table class="data display datatable" id="example">
					<thead>
						<tr>
							<th>Id</th>
							<th>Date & Time</th>
              <th>Product Name</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Customer Id</th>
              <th>Address</th>
              <th>Action</th>
						</tr>
					</thead>
					<tbody>
                    <?php
                      $getOrder = $cart->getAllOrderedProduct();
                      if ($getOrder){
                          while ($result = $getOrder->fetch_assoc()){

                    ?>
						<tr class="odd gradeX">
							<td><?php echo $result['id'];?></td>
							<td><?php echo $fm->formatDate($result['date']);?></td>
                            <td><?php echo $result['productName'];?></td>
                            <td><?php echo $result['quantity'];?></td>
                            <td>$<?php echo $result['price'];?></td>
                            <td><?php echo $result['cmrId'];?></td>
                            <td><a href="customer.php?cmrId=<?php echo $result['cmrId'];?>">View Details</a></td>
							<td>
                                <?php
                                   if ($result['status'] == '0'){ ?>
                                       <a href="?recieveid=<?php echo $result['cmrId'];?> && price=<?php echo $result['price'];?> && time=<?php echo $result['date'];?>">Recieve</a>
                                  <?php }elseif ($result['status'] == '1'){ ?>
                                        <b>Pending</b>
                                <?php }else{ ?>
                                    <a href="?removeid=<?php echo $result['cmrId'];?> && price=<?php echo $result['price'];?> && time=<?php echo $result['date'];?>">Remove</a>
                                <?php } ?>
                            </td>
						</tr>
                     <?php } }?>
					</tbody>
				</table>
               </div>
            </div>
        </div>
<script type="text/javascript">
    $(document).ready(function () {
        setupLeftMenu();

        $('.datatable').dataTable();
        setSidebarHeight();
    });
</script>
<?php include 'inc/footer.php';?>
