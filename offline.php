<?php include "inc/header.php"?>

<?php
$getLogin = Session::get("cmrLogin");
if ($getLogin == false){
    header("Location:login.php");
}
?>
<?php 
  if (isset($_GET['orderid']) && $_GET['orderid'] == 'order'){
      $cmrId = Session::get("cmrId");
      $insertOrder = $cart->insertOrder($cmrId);
      $delcart = $cart->delCastomerCartData();
      header("Location:paymentsuccess.php");
  }
?>
<style type="text/css">
    .row{width: 100%;display: flex;}
    .divition{width: 50%;justify-content: space-between;padding: 5px;}
	.tblone{width: 100%;margin: 0 auto;border: 2px solid #ddd;}
    .tblone tr td {text-align: justify;}
    .tbltwo{float:right;text-align:left;width: 60%;border: 2px solid #ddd;margin-top: 10px;}
    .tbltwo tr:nth-child(even) {background-color: #fdf0f1}
    .ordernow{padding-bottom: 30px;}
    .ordernow a{width: 200px;margin: 20px auto 0;text-align: center;font-size: 30px;padding: 5px;display: block;background: #ff0000;color: #fff; border-radius: 3px;}
</style>
<div class="main">
	<div class="content">
		<div class="section-group">
			<div class="row">
				<div class="divition">
					<table class="tblone">
						<tr>
	                        <th>No.</th>
							<th>Name</th>
							<th>Price</th>
							<th >Quantity</th>
							<th>Total Price</th>
						</tr>
	                    <?php
	                        $getCPro = $cart->getCartProduct();
	                        if ($getCPro){
	                            $i = 0;
	                            $quantity = 0;
	                            $sum = 0;
	                            while ($result = $getCPro->fetch_assoc()){
	                                $i++;
	                    ?>
						<tr>
	                        <td><?php echo $i;?></td>
							<td><?php echo $result['productName'];?></td>
							<td>$<?php echo $result['price'];?></td>
							<td><?php echo $result['quantity'];?></td>
	                        <td>$<?php
	                            $total = $result['price'] * $result['quantity'];
	                            echo $total;
	                            ?>	
	                        </td>
						</tr>
	                     <?php
	                         $quantity = $quantity + $result['quantity'];
	                         $sum = $sum + $total;
	                         
	                     ?>
						<?php } }?>
					</table>
					<table class="tbltwo" >
						<tr>
							<th>Sub Total  </th>
							<td>: =</td>
							<td>$<?php echo $sum;?></td>
						</tr>
						<tr>
							<th>VAT  (10%)</th>
							<td>: =</td>
							<td>$<?php
	                              $vat = $sum * 0.1;
	                              echo $vat;
	                            ?>
	                        </td>
						</tr>
						<tr>
							<th>Grand Total </th>
							<td>: =</td>
							<td>$<?php
	                            $grandTotal = $sum + $vat;
	                            echo $grandTotal;
							?></td>
						</tr>
				   </table>
				</div>
				<div class="divition">
					<?php
		               $id = Session::get("cmrId");
		               $getData = $cmr->getAllCustomerData($id);

		               if ($getData){
		                   while ($result = $getData->fetch_assoc()){

		            ?>
					<table class="tblone">
		                <tr>
		                    <td colspan="3" style="text-align: center"><h2>User Profile</h2></td>
		                </tr>
		                <tr>
		                    <td width="20%">Name</td>
		                    <td width="5%">:</td>
		                    <td><?php echo $result['name']?></td>
		                </tr>
		                <tr>
		                    <td>City</td>
		                    <td>:</td>
		                    <td><?php echo $result['city']?></td>
		                </tr>
		                <tr>
		                    <td>Country</td>
		                    <td>:</td>
		                    <td><?php echo $result['country']?></td>
		                </tr>
		                <tr>
		                    <td>Zip Code</td>
		                    <td>:</td>
		                    <td><?php echo $result['zip']?></td>
		                </tr>
		                <tr>
		                    <td>Phone</td>
		                    <td>:</td>
		                    <td><?php echo $result['phone']?></td>
		                </tr>
		                <tr>
		                    <td>Email</td>
		                    <td>:</td>
		                    <td><?php echo $result['email']?></td>
		                </tr>
		                <tr>
		                    <td>Address</td>
		                    <td>:</td>
		                    <td><?php echo $result['adress']?></td>
		                </tr>
		                <tr>
		                    <td></td>
		                    <td></td>
		                    <td><a href="editprofile.php">Update Profile</a></td>
		                </tr>
		            </table>
		           <?php } }?>
				</div>
			</div>
		</div>
	</div>
	<div class="ordernow"><a href="?orderid=order">ORDER</a></div>
</div>

<?php include "inc/footer.php"?>
