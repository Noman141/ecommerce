<?php include "inc/header.php"?>

<?php
$getLogin = Session::get("cmrLogin");
if ($getLogin == false){
    header("Location:login.php");
}
?>
<style type="text/css">
    .psuccess{width: 500px;height: 400px;text-align: center;border: 1px solid #ddd;margin: 0 auto;padding: 20px;}
    .psuccess h2{border-bottom: 1px solid #ddd;margin-bottom: 20px;padding-bottom: 10px;}
    .payment p{line-height: 20px;font-size: 18px;text-align: left!important;}
</style>
<div class="main">
    <div class="content">
        <div class="section-group">
            <div class="psuccess">
                <h2>Success</h2>
                <p>Total Payable Amount (including 10% vat): $
                <?php
                $cmrId = Session::get('cmrId');
                   $amount = $cart->payableAmount($cmrId);
                   if ($amount){
                       $sum = 0;
                       while ($result = $amount->fetch_assoc()){
                           $price = $result['price'];
                           $sum = $price + $sum;
                       }
                   }
                $vat = $sum * 0.1;
                $total = $sum + $vat;
                echo $total;
                   ?>
                </p>
                <p>Thank you for Purchase. Your order is successfull. We will contact you as soon as possible with delivery details. To check your order details please click here <a href="orderdetails.php">View Order Details</a></p>
            </div>
        </div>
    </div>
</div>

<?php include "inc/footer.php"?>
