<?php include "inc/header.php"?>
<?php 
  if (!isset($_GET['catId']) AND $_GET['catId'] == NULL ){
   echo "<script>window.location = 'catlist.php';</script>";
}else{
   $id = preg_replace('/[^-a-zA-Z0-9_]/','',$_GET['catId']);
}
  
?>
 <div class="main">
    <div class="content">
    	<div class="content_top">
    		<div class="heading">
    		<h3>Latest from Catagory</h3>
    		</div>
    		<div class="clear"></div>
    	</div>
	      <div class="section group">
            <?php
               $getProbycat = $pro->getProductByCatagory($id);
               if ($getProbycat){
                   while ($proByCat = $getProbycat->fetch_assoc()){

            ?>
				<div class="grid_1_of_4 images_1_of_4">
					 <a href="preview.php?proId=<?php echo $proByCat['productId']?>"><img src="admin/<?php echo $proByCat['image']?>" alt="" /></a>
					 <h2><?php echo $proByCat['productName']?> </h2>
					 <p><?php echo $fm->textShorten($proByCat['body'],50)?></p>
					 <p><span class="price"><?php echo $proByCat['price']?></span></p>
				     <div class="button"><span><a href="preview.php?proId=<?php echo $proByCat['productId']?>" class="details">Details</a></span></div>
				</div>
		    <?php } }else{
                   header("Location:404.php");
               }?>
			</div>

	
	
    </div>
 </div>
<?php include "inc/footer.php"?>

