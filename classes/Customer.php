<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/5/2018
 * Time: 10:56 AM
 */

class Customer{
    private $db;
    private $fm;
    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function customerRegistration($data){
        $name = $this->fm->validation($data['name']);
        $city = $this->fm->validation($data['city']);
        $country = $this->fm->validation($data['country']);
        $zip = $this->fm->validation($data['zipcode']);
        $phone = $this->fm->validation($data['phone']);
        $email = $this->fm->validation($data['email']);
        $password = $this->fm->validation($data['password']);
        $adress = $this->fm->validation($data['adress']);

        $name = mysqli_real_escape_string($this->db->link,$name);
        $city = mysqli_real_escape_string($this->db->link,$city);
        $country = mysqli_real_escape_string($this->db->link,$country);
        $zip = mysqli_real_escape_string($this->db->link,$zip);
        $phone = mysqli_real_escape_string($this->db->link,$phone);
        $email = mysqli_real_escape_string($this->db->link,$email);
        $password = mysqli_real_escape_string($this->db->link,$password);
        $adress = mysqli_real_escape_string($this->db->link,$adress);

        if ($name == "" || $city == "" || $zip == "" || $email == "" || $adress == "" || $country == "" || $phone == "" || $password == "" ){
            $msg = "<span style='color: red'>Fields Must Not Be Empty!</span>";
            return $msg;
        }
        $password = md5($password);

        $mailquery = "SELECT * FROM tbl_customer WHERE email = '$email' LIMIT 1";
        $mailchk = $this->db->select($mailquery);
        if ($mailchk != false){
            $msg = "<span style='color: red'>This Email Is Already Exsist!</span>";
            return $msg;
        }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $msg = "<span style='color: red'>This Email Is Not Valid!</span>";
            return $msg;
        }else{
            $sql = "INSERT INTO tbl_customer(name,city,country,zip,phone,email,password,adress) VALUES ('$name','$city','$country','$zip','$phone','$email','$password','$adress') ";
            $inserted_row = $this->db->insert($sql);
            if ($inserted_row){
                $msg = "<span style='color: green'>Data Inserted Successfully</span>";
                return $msg;
            }else{
                $msg = "<span style='color: red'>Data Not Inserted!</span>";
                return $msg;
            }
        }
    }

    public function customerLogIn($data){
        $email = $this->fm->validation($data['email']);
        $password = $this->fm->validation($data['password']);

        $email = mysqli_real_escape_string($this->db->link,$email);
        $password = mysqli_real_escape_string($this->db->link,md5($password));

        if ($email == "" || $password == ""){
            $msg = "<span style='color: red'>Fields Must Not Be Empty!</span>";
            return $msg;
        }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $msg = "<span style='color: red'>This Email Is Not Valid!</span>";
            return $msg;
        }else{
            $sql = "SELECT * FROM tbl_customer WHERE email = '$email' AND password = '$password'";
            $result = $this->db->select($sql);
            if ($result != false){
                $value  = $result->fetch_assoc();
                Session::set("cmrLogin",true);
                Session::set("cmrId",$value['id']);
                Session::set("cmrName",$value['name']);
                header("Location:cart.php");
            }else{
                $msg = "<span style='color: red'>Email OR Password Not Matched!</span>";
                return $msg;
            }
        }
    }

    public function getAllCustomerData($id){
        $sql = "SELECT * FROM tbl_customer WHERE id = '$id'";
        $result = $this->db->select($sql);
        return $result;
    }

    public function updateCustomerData($data,$cmrId){
        $name = $this->fm->validation($data['name']);
        $city = $this->fm->validation($data['city']);
        $country = $this->fm->validation($data['country']);
        $zip = $this->fm->validation($data['zip']);
        $phone = $this->fm->validation($data['phone']);
        $email = $this->fm->validation($data['email']);
        $adress = $this->fm->validation($data['adress']);

        $name = mysqli_real_escape_string($this->db->link,$name);
        $city = mysqli_real_escape_string($this->db->link,$city);
        $country = mysqli_real_escape_string($this->db->link,$country);
        $zip = mysqli_real_escape_string($this->db->link,$zip);
        $phone = mysqli_real_escape_string($this->db->link,$phone);
        $email = mysqli_real_escape_string($this->db->link,$email);
        $adress = mysqli_real_escape_string($this->db->link,$adress);

        if ($name == "" || $city == "" || $zip == "" || $email == "" || $adress == "" || $country == "" || $phone == "" ){
            $msg = "<span style='color: red'>Fields Must Not Be Empty!</span>";
            return $msg;
        }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $msg = "<span style='color: red'>This Email Is Not Valid!</span>";
            return $msg;
        }else{
            $sql = "UPDATE tbl_customer
                        SET
                            name = '$name',
                            city = '$city',
                            country = '$country',
                            zip = '$zip',
                            phone = '$phone',
                            email = '$email',
                            adress = '$adress'
                        WHERE id = '$cmrId'";
            $updated_row = $this->db->update($sql);
            if ($updated_row){
                $msg = "<span style='color: green'>Data Updated Successfully</span>";
                return $msg;
            }else{
                $msg = "<span style='color: red'>Data Not Updated</span>";
                return $msg;
            }
        }
    }
}