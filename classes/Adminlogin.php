<?php
  include '../lib/Session.php';
  Session::checkLogin();
  include_once '../lib/Database.php';
  include_once '../helpers/Format.php';
?>

<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/1/2018
 * Time: 8:10 PM
 */

class Adminlogin{
    private $db;
    private $fm;
    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function adminLogin($adminUser,$adminPassword){

        $adminUser = $this->fm->validation($adminUser);
        $adminPassword = $this->fm->validation($adminPassword);

        $adminUser = mysqli_real_escape_string($this->db->link,$adminUser);
        $adminPassword = mysqli_real_escape_string($this->db->link,$adminPassword);

        if (empty($adminUser) || empty($adminPassword)){
           $loginmsg = "UserName and Password Must Not Be Empty!";
           return $loginmsg;
        }else{
            $query = "SELECT * FROM tbl_admin WHERE adminUser = '$adminUser' AND adminPassword = '$adminPassword'";
            $result  = $this->db->select($query);
            if ($result != false){
                $value = $result->fetch_assoc();
                Session::set("adminlogin",true);
                Session::set("adminId",$value['adminId']);
                Session::set("adminName",$value['adminName']);
                Session::set("adminUser",$value['adminUser']);
                header("Location:index.php");
            }else{
                $loginmsg = "UserName and Password Not Match!!";
                return $loginmsg;
            }
        }
    }
}