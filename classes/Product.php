<?php
//include_once '../lib/Database.php';
//include_once '../helpers/Format.php';
//require_once(realpath(dirname(__FILE__) . '/../helpers/Format.php'));
?>
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/4/2018
 * Time: 11:33 AM
 */

class Product{
    private $db;
    private $fm;
    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function productInsert($data,$file){
        $productName = $this->fm->validation($data['productName']);
        $catId = $this->fm->validation($data['catId']);
        $brandId = $this->fm->validation($data['brandId']);
        $body = $this->fm->validation($data['body']);
        $price = $this->fm->validation($data['price']);
        $type = $this->fm->validation($data['type']);

        $productName = mysqli_real_escape_string($this->db->link,$productName);
        $catId = mysqli_real_escape_string($this->db->link,$catId);
        $brandId = mysqli_real_escape_string($this->db->link,$brandId);
        $body = mysqli_real_escape_string($this->db->link,$body);
        $price = mysqli_real_escape_string($this->db->link,$price);
        $type = mysqli_real_escape_string($this->db->link,$type);

        $permited = array('jpg','png','jpeg','gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.',$file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()),0,10).'.'.$file_ext;
        $uploaded_image = "uploads/".$unique_image;

        if ($productName == "" || $catId == "" || $brandId == "" || $body == "" || $price == "" || $type == "" || $file_name == ""){
            $msg = "<span style='color: red'>Fields Must Not Be Empty!</span>";
            return $msg;
        }elseif ($file_size >1048567) {
            $msg = "<span style='color: red'>Image Size should be less then 1MB!</span>";
            return $msg;
        } elseif (in_array($file_ext, $permited) === false) {
            $msg = "style='color: red'>You can upload only:-" .implode(', ', $permited)."</span>";
            return $msg;
        }else{
            move_uploaded_file($file_temp, $uploaded_image);
            $query = "INSERT INTO tbl_product(productName,catId,brandId,body,price,image,type) VALUES('$productName','$catId','$brandId','$body','$price','$uploaded_image','$type')";
            $product_insert = $this->db->insert($query);
            if ($product_insert){
                $msg = "<span style='color: green'>Product Inserted Successfully</span>";
                return $msg;
            }else{
                $msg = "<span style='color: red'>Product Not Inserted!</span>";
                return $msg;
            }
        }
    }

    public function getAllProduct(){
        $sql = "SELECT tbl_product.*, tbl_catagory.catName, tbl_brand.brandName
                FROM tbl_product
                INNER JOIN tbl_catagory
                ON tbl_product.catId = tbl_catagory.catId
                INNER JOIN tbl_brand
                ON tbl_product.brandId = tbl_brand.brandId
        
        ORDER BY tbl_product.productId DESC";
        $getProduct = $this->db->select($sql);
        return $getProduct;
    }

    public function getAllProductById($proId){
        $sql = "SELECT * FROM tbl_product WHERE productId = '$proId'";
        $getPro = $this->db->select($sql);
        return $getPro;
    }

    public function productUpdate($data,$file,$proId){
        $productName = $this->fm->validation($data['productName']);
        $catId = $this->fm->validation($data['catId']);
        $brandId = $this->fm->validation($data['brandId']);
        $body = $this->fm->validation($data['body']);
        $price = $this->fm->validation($data['price']);
        $type = $this->fm->validation($data['type']);

        $productName = mysqli_real_escape_string($this->db->link,$productName);
        $catId = mysqli_real_escape_string($this->db->link,$catId);
        $brandId = mysqli_real_escape_string($this->db->link,$brandId);
        $body = mysqli_real_escape_string($this->db->link,$body);
        $price = mysqli_real_escape_string($this->db->link,$price);
        $type = mysqli_real_escape_string($this->db->link,$type);

        $permited = array('jpg','png','jpeg','gif');
        $file_name = $file['image']['name'];
        $file_size = $file['image']['size'];
        $file_temp = $file['image']['tmp_name'];

        $div = explode('.',$file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()),0,10).'.'.$file_ext;
        $uploaded_image = "uploads/".$unique_image;

        if ($productName == "" || $catId == "" || $brandId == "" || $body == "" || $price == "" || $type == ""){
            $msg = "<span style='color: red'>Fields Must Not Be Empty!</span>";
            return $msg;
        }else{
            if (!empty($file_name)) {
               if ($file_size >1048567) {
                $msg = "<span style='color: red'>Image Size should be less then 1MB!</span>";
                return $msg;
            } elseif (in_array($file_ext, $permited) === false) {
                $msg = "<span style='color: red'>You can upload only:-" .implode(', ', $permited)."</span>";
                return $msg;
            }else{
                move_uploaded_file($file_temp, $uploaded_image);
                $sql = "UPDATE tbl_product
                        SET
                            productName = '$productName',
                            catId = '$catId',
                            brandId = '$brandId',
                            body = '$body',
                            price = '$price',
                            image = '$uploaded_image',
                            type = '$type'
                        WHERE productId = '$proId'";
                $updated_row = $this->db->update($sql);
                if ($updated_row){
                    $msg = "<span style='color: green'>Product Updated Successfully</span>";
                    return $msg;
                }else{
                    $msg = "<span style='color: red'>Product Not Updated</span>";
                    return $msg;
                }
            }
        }else{
            $sql = "UPDATE tbl_product
                    SET
                        productName = '$productName',
                        catId = '$catId',
                        brandId = '$brandId',
                        body = '$body',
                        price = '$price',
                        type = '$type'
                    WHERE productId = '$proId'";
            $updated_row = $this->db->update($sql);
            if ($updated_row){
                $msg = "<span style='color: green'>Product Updated Successfully</span>";
                return $msg;
            }else{
                $msg = "<span style='color: red'>Product Not Updated</span>";
                return $msg;
            }
          }
        }
    }

    public function deleteProduct($delProId){
        $query = "SELECT * FROM tbl_product WHERE productId = '$delProId'";
        $getdelimg = $this->db->delete($query);
        if ($getdelimg){
            while ($delimg = $getdelimg->fetch_assoc()){
                $delimglink = $delimg['image'];
                unlink($delimglink);
            }
        }

        $sql = "DELETE FROM tbl_product WHERE productId = '$delProId'";
        $delproduct = $this->db->delete($sql);
        if ($delproduct){
            $msg = "<span style='color: green'>Product Deleted Successfully</span>";
            return $msg;
        }else{
            $msg = "<span style='color: red'>Product Not Deleted</span>";
            return $msg;
        }
    }

    public function getFeaturedProduct(){
        $sql = "SELECT * FROM tbl_product WHERE type ='0' ORDER BY productId DESC LIMIT 4";
        $result = $this->db->select($sql);
        return $result;
    }

    public function getNewProduct(){
        $sql = "SELECT * FROM tbl_product ORDER BY productId DESC LIMIT 4";
        $result = $this->db->select($sql);
        return $result;
    }

    public function getSingleProduct($id){
        $sql = "SELECT p.*,c.catName,b.brandName
                FROM tbl_product as p,tbl_catagory as c,tbl_brand as b
                WHERE p.catId = c.catId AND p.brandId = b.brandId AND p.productId = '$id'";
        $result = $this->db->select($sql);
        return $result;
    }

    public function latestFromIphone(){
        $sql = "SELECT * FROM tbl_product WHERE brandId = '5' ORDER BY  productId DESC LIMIT 1";
        $result = $this->db->select($sql);
        return $result;
    }

    public function latestFromSamsung(){
        $sql = "SELECT * FROM tbl_product WHERE brandId = '8' ORDER BY  productId DESC LIMIT 1";
        $result = $this->db->select($sql);
        return $result;
    }

    public function latestFromCanon(){
        $sql = "SELECT * FROM tbl_product WHERE brandId = '10' ORDER BY  productId DESC LIMIT 1";
        $result = $this->db->select($sql);
        return $result;
    }

    public function latestFromLenovo(){
        $sql = "SELECT * FROM tbl_product WHERE brandId = '3' ORDER BY  productId DESC LIMIT 1";
        $result = $this->db->select($sql);
        return $result;
    }

    public function getProductByCatagory($id){
        $id = mysqli_real_escape_string($this->db->link,$id);
        $sql = "SELECT * FROM tbl_product WHERE catId = '$id'";
        $result = $this->db->select($sql);
        return $result;
    }

    public function insertCompareData($productId,$cmrId){
        $cmrId = mysqli_real_escape_string($this->db->link,$cmrId);
        $productId = mysqli_real_escape_string($this->db->link,$productId);

        $query = "SELECT * FROM tbl_product WHERE productId = '$productId'";
        $result = $this->db->select($query)->fetch_assoc();

        $cquery = "SELECT * FROM tbl_compare WHERE cmrId = '$cmrId' AND productId = '$productId'";
        $chkcompare = $this->db->select($cquery);
        if ($chkcompare){
            $msg = "<span style='color: red'>Alredy Added To Compare</span>";
            return $msg;
        }

        if ($result){
            $productId = $result['productId'];
            $productName = $result['productName'];
            $price = $result['price'];
            $image = $result['image'];

            $sql = "INSERT INTO tbl_compare(cmrId,productId,productName,price,image) VALUES ('$cmrId','$productId','$productName','$price ','$image')";
            $inserted_row = $this->db->insert($sql);
            if ($inserted_row){
                $msg = "<span style='color: green'>Added To Compare</span>";
                return $msg;
            }else{
                $msg = "<span style='color: red'>Not Added To Compare</span>";
                return $msg;
            }
        }
    }

    public function getCompareProduct($cmrId){
        $sql = "SELECT * FROM tbl_compare WHERE cmrId = '$cmrId'";
        $result = $this->db->select($sql);
        return $result;
    }

    public function deleteCompareData($cmrId){
        $sql = "DELETE FROM tbl_compare WHERE cmrId = '$cmrId'";
        $delproduct = $this->db->delete($sql);
    }

    public function insertWishListData($id,$cmrId){
        $cquery = "SELECT * FROM tbl_wlist WHERE cmrId = '$cmrId' AND productId = '$id'";
        $chkcompare = $this->db->select($cquery);
        if ($chkcompare){
            $msg = "<span style='color: red'>Alredy Added To Compare</span>";
            return $msg;
        }
        $sql ="SELECT * FROM tbl_product WHERE productId = '$id'";
        $result = $this->db->select($sql)->fetch_assoc();
        if ($result){
                $productId = $result['productId'];
                $productName = $result['productName'];
                $price = $result['price'];
                $image = $result['image'];

                $sql = "INSERT INTO tbl_wlist(cmrId,productId,productName,price,image) VALUES('$cmrId','$productId','$productName','$price','$image')";
                $inserted_row = $this->db->insert($sql);
                if ($inserted_row){
                    $msg = "<span style='color: green'>Added To Wish List</span>";
                    return $msg;
                }else{
                    $msg = "<span style='color: red'>Not Added To Wish List</span>";
                    return $msg;
                }
            }

    }

    public function getWishListProduct($cmrId){
        $sql = "SELECT * FROM tbl_wlist WHERE cmrId = '$cmrId' ORDER BY id DESC ";
        $result = $this->db->select($sql);
        return $result;
    }

    public function deleteWishListData($proId,$cmrId){
        $sql = "DELETE FROM tbl_wlist WHERE cmrId = '$cmrId' AND productId = '$proId'";
        $delproduct = $this->db->delete($sql);
        if ($delproduct){
            $msg = "<span style='color: green'>Successfully Removed</span>";
            return $msg;
        }else{
            $msg = "<span style='color: red'>Not Removed</span>";
            return $msg;
        }
    }

}