<?php
//include_once 'lib/Database.php';
//include_once 'helpers/Format.php';
?>

<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/5/2018
 * Time: 10:56 AM
 */

class Cart{
    private $db;
    private $fm;
    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function addToCart($quantity,$id){
        $quantity = $this->fm->validation($quantity);
        $quantity = mysqli_real_escape_string($this->db->link,$quantity);
        $productId = mysqli_real_escape_string($this->db->link,$id);
        $sId = session_id();

        $query = "SELECT * FROM tbl_product WHERE productId = '$productId'";
        $result = $this->db->select($query)->fetch_assoc();

        $productName = $result['productName'];
        $price = $result['price'];
        $image = $result['image'];

        $chkquery = "SELECT * FROM tbl_cart WHERE productId = '$productId' AND sId = '$sId'";
        $getPro = $this->db->select($chkquery);
        if ($getPro){
            $msg = "<span style='color: red'>Producted Already Added To Cart</span>";
            return $msg;
        }else {
            $sql = "INSERT INTO tbl_cart(sId,productId,productName,price,quantity,image) VALUES('$sId','$productId','$productName','$price','$quantity','$image')";
            $inserted_row = $this->db->insert($sql);
            if ($inserted_row) {
                header("Location:cart.php");
            } else {
                header("Location:404.php");
            }
        }

    }

    public function getCartProduct(){
        $sId = session_id();
        $sql ="SELECT * FROM tbl_cart WHERE sId = '$sId'";
        $result = $this->db->select($sql);
        return $result;
    }

    public function updateCartQuantity($cartId,$quantity){
        $cartId = $this->fm->validation($cartId);
        $quantity = $this->fm->validation($quantity);

        $cartId =mysqli_real_escape_string($this->db->link,$cartId);
        $quantity =mysqli_real_escape_string($this->db->link,$quantity);

        $query = "UPDATE tbl_cart SET quantity = '$quantity' WHERE cartId = '$cartId'";
        $updated_row = $this->db->update($query);

        if ($updated_row){
            header("Location:cart.php");
        }else{
            $msg = "<span style='color: red'>Quantity Not Updated</span>";
            return $msg;
        }
    }

    public function deleteProductByCart($id){
        $id =mysqli_real_escape_string($this->db->link,$id);
        $query = "DELETE FROM tbl_cart WHERE cartId = '$id'";
        $delData = $this->db->delete($query);
        if ($delData){
//            $msg = "<span style='color: green'>Cart Deleted Successfully</span>";
//            return $msg;
            echo "<script>window.location='cart.php'</script>";
        }else{
            $msg = "<span style='color: red'>Cart Not Deleted</span>";
            return $msg;
        }
    }

    public function checkCartTable(){
        $sId = session_id();
        $sql ="SELECT * FROM tbl_cart WHERE sId = '$sId'";
        $result = $this->db->select($sql);
        return $result;
    }

    public function delCastomerCartData(){
        $sId = session_id();
        $sql = "DELETE FROM tbl_cart WHERE sId = '$sId'";
        $result = $this->db->delete($sql);
        return $result;
    }

    public function checkCustomerCart(){
        $sId = session_id();
        $sql ="SELECT * FROM tbl_cart WHERE sId = '$sId'";
        $result = $this->db->select($sql);
        return $result;
    }

    public function insertOrder($cmrId){
        $sId = session_id();
        $sql ="SELECT * FROM tbl_cart WHERE sId = '$sId'";
        $getPro = $this->db->select($sql);
        if ($getPro){
            while ($result = $getPro->fetch_assoc()){
                $productId = $result['productId'];
                $productName = $result['productName'];
                $quantity = $result['quantity'];
                $price = $result['price'] * $quantity;
                $image = $result['image'];

                $sql = "INSERT INTO tbl_order(cmrId,productId,productName,quantity,price,image) VALUES('$cmrId','$productId','$productName','$quantity','$price','$image')";
                $inserted_row = $this->db->insert($sql);
            }
        }
    }

    public function payableAmount($cmrId){
        $sql ="SELECT price FROM tbl_order WHERE cmrId = '$cmrId' AND date = NOW()";
        $result = $this->db->select($sql);
        return $result;
    }

    public function getOrderedProduct($cmrId){
        $sql ="SELECT * FROM tbl_order WHERE cmrId = '$cmrId' ORDER BY date DESC";
        $result = $this->db->select($sql);
        return $result;
    }

    public function checkOrder($cmrId){
        $sql ="SELECT * FROM tbl_order WHERE cmrId = '$cmrId'";
        $result = $this->db->select($sql);
        return $result;
    }

    public function getAllOrderedProduct(){
        $sql ="SELECT * FROM tbl_order ORDER BY date";
        $result = $this->db->select($sql);
        return $result;
    }

    public function productReceived($id,$price,$date){
        $id = mysqli_real_escape_string($this->db->link,$id);
        $price = mysqli_real_escape_string($this->db->link,$price);
        $date = mysqli_real_escape_string($this->db->link,$date);

        $sql = "UPDATE tbl_order
                SET
                status = '1'
                WHERE cmrId = '$id' AND date ='$date' AND price = '$price'";
        $updated_row = $this->db->update($sql);

        if ($updated_row){
            $msg = "<span style='color: green'> Updated Successfully</span>";
            return $msg;
        }else{
            $msg = "<span style='color: red'> Not Updated</span>";
            return $msg;
        }
    }

    public function productRemove($id,$price,$date){
        $id = mysqli_real_escape_string($this->db->link,$id);
        $price = mysqli_real_escape_string($this->db->link,$price);
        $date = mysqli_real_escape_string($this->db->link,$date);

        $sql = "DELETE FROM tbl_order WHERE cmrId = '$id' AND date ='$date' AND price = '$price'";
        $removeData = $this->db->delete($sql);
        if ($removeData){
            $msg = "<span style='color: green'> Data Deleted Successfully</span>";
            return $msg;
        }else{
            $msg = "<span style='color: red'>Data Not Deleted</span>";
            return $msg;
        }
    }

    public function productReceivedConfirm($id,$price,$date){
        $id = mysqli_real_escape_string($this->db->link,$id);
        $price = mysqli_real_escape_string($this->db->link,$price);
        $date = mysqli_real_escape_string($this->db->link,$date);

        $sql = "UPDATE tbl_order
                SET
                status = '2'
                WHERE cmrId = '$id' AND date ='$date' AND price = '$price'";
        $updated_row = $this->db->update($sql);

        if ($updated_row){
            $msg = "<span style='color: green'> Updated Successfully</span>";
            return $msg;
        }else{
            $msg = "<span style='color: red'> Not Updated</span>";
            return $msg;
        }
    }

}