<?php
require_once(realpath(dirname(__FILE__) . '/../helpers/Format.php'));
?>

<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/3/2018
 * Time: 11:11 PM
 */

class Brand{
    private $db;
    private $fm;
    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function brandAdd($brandName){
        $brandName = $this->fm->validation($brandName);
        $brandName = mysqli_real_escape_string($this->db->link,$brandName);

        if (empty($brandName)){
            $msg = "<span style='color: red'>Field Must Not Be Empty</span>";
            return $msg;
        }else{
            $query = "INSERT INTO tbl_brand(brandName) VALUES('$brandName')";
            $addbrand = $this->db->insert($query);
            if ($addbrand){
                $msg = "<span style='color: green'>Brand Added Successfully</span>";
                return $msg;
            }else{
                $msg = "<span style='color: red'>Brand Not Added</span>";
                return $msg;
            }
        }
    }

    public function getAllBrand(){
        $query = "SELECT * FROM tbl_brand ORDER BY brandId DESC ";
        $getBrand = $this->db->select($query);
        return $getBrand;
    }

    public function getBrandById($brandid){
        $query = "SELECT * FROM tbl_brand WHERE brandId = '$brandid'";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateBrandName($brandName,$brandid){
        $brandName = $this->fm->validation($brandName);
        $brandName = mysqli_real_escape_string($this->db->link,$brandName);
        $brandid = mysqli_real_escape_string($this->db->link,$brandid);

        if (empty($brandName)){
            $msg = "<span style='color: red'>Field Must Not Be Empty</span>";
            return $msg;
        }else{
            $query = "UPDATE tbl_brand SET brandName = '$brandName' WHERE brandId = '$brandid'";
            $updated_row = $this->db->update($query);

            if ($updated_row){
                $msg = "<span style='color: green'>Brand Name Updated Successfully</span>";
                return $msg;
            }else{
                $msg = "<span style='color: red'>Brand Name Not Updated</span>";
                return $msg;
            }
        }

    }

    public function deleteBrandById($delbrandid){
        $query = "DELETE FROM tbl_brand WHERE brandId = '$delbrandid'";
        $delData = $this->db->delete($query);
        if ($delData){
            $msg = "<span style='color: green'>Brand Name Deleted Successfully</span>";
            return $msg;
            header("Location:'brandlist.php'");
        }else{
            $msg = "<span style='color: red'>Brand Name Not Deleted</span>";
            return $msg;
        }
    }
}