<?php require_once(realpath(dirname(__FILE__) . '/../helpers/Format.php')); ?>
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 10/3/2018
 * Time: 10:57 AM
 */

class Catagory{
    private $db;
    private $fm;
    public function __construct(){
        $this->db = new Database();
        $this->fm = new Format();
    }

    public function catagoryAdd($catName){
        $catName = $this->fm->validation($catName);
        $catName = mysqli_real_escape_string($this->db->link,$catName);

        if (empty($catName)){
            $msg = "<span style='color: red'>Catagory Field Must Not Be Empty</span>";
            return $msg;
        }else{
            $query = "INSERT INTO tbl_catagory(catName) VALUES('$catName') ";
            $addCat = $this->db->insert($query);
            if ($addCat){
                $msg = "<span style='color: green'>Catagory Added Successfully</span>";
                return $msg;
            }else{
                $msg = "<span style='color: red'>Catagory Not Added</span>";
                return $msg;
            }
        }
    }

    public function getAllCat(){
        $query = "SELECT * FROM tbl_catagory ORDER BY catId DESC ";
        $getCat = $this->db->select($query);
        return $getCat;
    }

    public function getCatById($catid){
        $query = "SELECT * FROM tbl_catagory WHERE catId = '$catid'";
        $result = $this->db->select($query);
        return $result;
    }

    public function updateCatagory($catName,$catid){
        $catName = $this->fm->validation($catName);
        $catName = mysqli_real_escape_string($this->db->link,$catName);
        $catid = mysqli_real_escape_string($this->db->link,$catid);

        if (empty($catName)){
            $msg = "<span style='color: red'>Catagory Field Must Not Be Empty</span>";
            return $msg;
        }else{
            $query = "UPDATE tbl_catagory SET catName = '$catName' WHERE catId = '$catid'";
            $updated_row = $this->db->update($query);

            if ($updated_row){
                $msg = "<span style='color: green'>Catagory Updated Successfully</span>";
                return $msg;
            }else{
                $msg = "<span style='color: red'>Catagory Not Updated</span>";
                return $msg;
            }
        }

    }

    public function deleteCatagoryById($delcatid){
        $query = "DELETE FROM tbl_catagory WHERE catId = '$delcatid'";
        $delData = $this->db->delete($query);
        if ($delData){
            $msg = "<span style='color: green'>Catagory Deleted Successfully</span>";
            return $msg;
            header("Location:'catlist.php'");
        }else{
            $msg = "<span style='color: red'>Catagory Not Deleted</span>";
            return $msg;
        }
    }
}