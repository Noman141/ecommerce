<?php include "inc/header.php"?>
<?php
   if (!isset($_GET['proId']) || $_GET['proId'] == NULL){
       echo "<script>window.location='404.php'</script>";
   }else{
       $id =preg_replace('/[^-a-zA-Z0-9_]/','',$_GET['proId']);

       if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['submit'])){
           $quantity = $_POST['quantity'];

           $addCart = $cart->addToCart($quantity,$id);
       }
   }
?>
<?php
$cmrId = Session::get("cmrId");
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['compare'])){
    $productId = $_POST['productId'];
    $insertCompare = $pro->insertCompareData($productId,$cmrId);
}

?>
<?php
$cmrId = Session::get("cmrId");
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['wlist'])){
    $insertWlist = $pro->insertWishListData($id,$cmrId);
}

?>
<style>
    .mybutton{width: 100px;margin-right: 50px;float: left}
</style>
 <div class="main">
    <div class="content">
    	<div class="section group">
				<div class="cont-desc span_1_of_2">
                <?php
                   $getProduct = $pro->getSingleProduct($id);
                   if ($getProduct){
                       while ($result = $getProduct->fetch_assoc()){

                ?>
					<div class="grid images_3_of_2">
						<img src="admin/<?php echo $result['image']?>" alt="" />
					</div>
				<div class="desc span_3_of_2" style="margin-bottom:10px ">
					<h2><?php echo $result['productName']?></h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</p>
					<div class="price">
						<p>Price: <span>$<?php echo $result['price']?></span></p>
						<p>Category: <span><?php echo $result['catName']?></span></p>
						<p>Brand:<span><?php echo $result['brandName']?></span></p>
					</div>
				<div class="add-cart">
					<form action="" method="post">
						<input type="number" class="buyfield" name="quantity" value="1"/>
						<input type="submit" class="buysubmit" name="submit" value="Buy Now"/>
					</form>				
				</div>
                    <span>
                        <?php
                        if (isset($addCart)){
                            echo $addCart;
                        }
                        ?>
                    </span>
			</div>
           <?php
           if (isset($insertCompare)){
               echo $insertCompare;
           }
           ?>
           <?php
                if (isset($insertWlist)){
                    echo $insertWlist;
                }
           ?>
            <?php
               $getLogin = Session::get("cmrLogin");
                  if ($getLogin == true){ ?>
           <div class="add-cart">
               <div class="mybutton">
                   <form action="" method="post">
                       <input type="hidden" class="buyfield" name="productId" value="<?php echo $result['productId']?>"/>
                       <input type="submit" class="buysubmit" name="compare" value="Add To Compare"/>
                   </form>
               </div>
               <div class="mybutton">
                   <form action="" method="post">
                       <input type="submit" class="buysubmit" name="wlist" value="Add To List"/>
                   </form>
               </div>
           </div>
           <?php }?>
			<div class="product-desc">
			<h2>Product Details</h2>
			<p><?php echo $result['body']?></p>
	    </div>
	<?php } }?>
	</div>
				<div class="rightsidebar span_3_of_1">
					<h2>CATEGORIES</h2>
					<ul>
                      <?php
                        $getCatagory = $cat->getAllCat();
                        if ($getCatagory){
                            while ($catagory = $getCatagory->fetch_assoc()){
                      ?>
				      <li><a href="productbycat.php?catId=<?php echo $catagory['catId']?>"><?php echo $catagory['catName']?></a></li>
				      <?php } }?>
    				</ul>
    	
 				</div>
 		</div>
 	</div>
	<?php include "inc/footer.php"?>